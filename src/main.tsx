import ReactDOM from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import { RoutesMain } from './routes/RoutesMain'
import '../src/assets/styles/_styles.main.css'
ReactDOM.createRoot(document.getElementById('root')!).render(
    <BrowserRouter>
    <RoutesMain />
    </BrowserRouter>
    
)

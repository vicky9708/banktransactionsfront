import { useEffect } from "react"
import { getTransactions } from "../../home/services/services.home"
import { useAppStore } from "../../../store/store"
import { shallow } from "zustand/shallow"
import { IFTransaction } from "../../../types/types.home"
import { TransactionCardInfo } from "./_transaction.main"
import { useTranslation } from "react-i18next"
import { TbCashBanknoteOff } from "react-icons/tb"

export const TransactionList = () => {
    const { transactions } = useAppStore((state) => ({ transactions: state.transactions }), shallow)
    const { t } = useTranslation("app")
    useEffect(() => {
        getTransactions();
    }, [])
    return (
        <>{transactions.length > 0 ?
            <div className="home-list__scroll">
                {transactions.map((transaction: IFTransaction, index: number) => { return (<TransactionCardInfo key={index} transaction={transaction} />) })}
            </div>
            :
            <div className=" justify-center flex dark:bg-[#1B1827] bg-[#1b18270d] rounded-xl mt-4 p-4">
                <div className="m-auto">
                    <div className="flex">
                    <TbCashBanknoteOff className="text-[200px] text-gray-700 m-auto" />
                    </div>                    
                    <p>{t("TRANSACTION.NO_FOUND")}</p>
                </div>

            </div>}
        </>

    )
}
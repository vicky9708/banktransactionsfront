import { shallow } from "zustand/shallow"
import { useAppStore } from "../../../store/store"
import { Transaction } from "../../../types/types.home"
import { addTransaction, updateTransaction } from "../services/services.transaction"
import { useEffect } from "react"
interface IFTransactionAddEdit {
    action: "Create" | "Edit",
    translation:any
}
export const TransactionAddEdit = (props: IFTransactionAddEdit) => {
    const { transaction, setTransaction,userInfo } = useAppStore((state) => ({ transaction: state.transaction, setTransaction: state.setTransaction,userInfo:state.userInfo }), shallow)
    const onChange = (event: any) => {
        const change = event.target
        setTransaction({ ...transaction, [change.name]: change.value })
    }
    const onSubmit=(event:any)=>{
        event.preventDefault()
        props.action==="Create"?addTransaction():updateTransaction()
    }
    
    useEffect(()=>{
        setTransaction({ ...transaction, userId:userInfo._id  })
    },[])
    return (<>
        <form onSubmit={onSubmit}>
            <label className="form-control w-full max-w-xs">
                <div className="label">
                    <span className="label-text">{props.translation("TRANSACTION.MODAL_EDIT_CREATE.LBL_TYPE")}</span>
                </div>
                <select disabled={props.action==="Edit"} onChange={onChange} value={transaction.type} name="type" className="select select-bordered">
                    <option value={Transaction.INPUT}>{props.translation("TRANSACTION.MODAL_EDIT_CREATE.OPTION_INPUT")}</option>
                    <option value={Transaction.OUTPUT}>{props.translation("TRANSACTION.MODAL_EDIT_CREATE.OPTION_OUTPUT")}</option>
                </select>
            </label>
            <div className="form-control">
                <label className="label">
                    <span className="label-text">{props.translation("TRANSACTION.MODAL_EDIT_CREATE.LBL_VALUE")}</span>
                </label>
                <input onChange={onChange} type="number" value={transaction.value} name="value" className="input input-bordered"  required />
            </div>
            <div className="form-control">
                <label className="label">
                    <span className="label-text">{props.translation("TRANSACTION.MODAL_EDIT_CREATE.LBL_DESCRIPTION")}</span>
                </label>
                <input onChange={onChange} type="text" value={transaction.description} name="description" className="input input-bordered" required />
            </div>
            <div className="form-control mt-6">
                <button type="submit" className="btn btn-primary">{props.translation("TRANSACTION.MODAL_EDIT_CREATE.BTN_OK")}</button>
            </div>
        </form>

    </>)
}
export interface IFTransaction{
    userId:string
    type:TypeTransaction;
    value:number;
    description:string;
    createDate?:Date;
    _id?:string;
}
export type TypeTransaction="Input"|"Output"
export enum Transaction{
    INPUT="Input",
    OUTPUT="Output"
}
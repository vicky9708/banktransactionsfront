import { TransactionAddEdit, TransactionList } from "./_transaction.main"
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import { useTranslation } from "react-i18next"
import { useAppStore } from "../../../store/store"
import { shallow } from "zustand/shallow"
import { useFormatNumber } from "../../../utils/utils.format"
export const TransactionLayout = () => {
  const { t } = useTranslation("app")
  const {userInfo,setTransaction}=useAppStore((state)=>({userInfo:state.userInfo,setTransaction:state.setTransaction}),shallow)
  const { format: formatNumber } = useFormatNumber({ prefix: "$" })

  const showSwal = () => {
    setTransaction({
      userId: "",
      type: "Input",
      value: 0,
      description: ""
    })
    withReactContent(Swal).fire({
      title: <i className="!text-gray-700">{t("TRANSACTION.MODAL_EDIT_CREATE.TITLE")}</i>,
      html: <TransactionAddEdit translation={t} action="Create" />,
      showConfirmButton:false,   
      showCloseButton:true   
    })
  }
  return (<div className="sm:m-16 m-4">
    <h1 className="text-xl">{t("TRANSACTION.WELCOME")}{userInfo.name}</h1>
    <div className="flex justify-between">
    <p className="self-center">{t("TRANSACTION.TITLE")}</p>
    <button onClick={showSwal} className="btn btn-primary">{t("TRANSACTION.BTN_ADD")}</button>
    </div>    
    <TransactionList />
    <div className="flex justify-between mt-4">      
      <h1 className="text-xl font-bold self-center">{t("TRANSACTION.CAPITAL")}</h1>
      <p className={`self-center text-2xl ${Number(userInfo.salary)>0?"!text-primary":"!text-error"}`}>{formatNumber(Number(userInfo.salary))}</p>      
    </div>
  </div>)
} 
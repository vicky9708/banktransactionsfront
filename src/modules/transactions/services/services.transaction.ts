import { restDelete, restPost, restPut } from "../../../services/services.axios"
import { useAppStore } from "../../../store/store"
import { IFTransaction } from "../../../types/types.home";
import { getTransactions } from "../../home/services/services.home";

export const addTransaction=async ()=>{
    const {transaction,setUserInfo,userInfo,setTransaction}=useAppStore.getState()
    const transactionTemp:IFTransaction={...transaction}
    delete transactionTemp._id;
    delete transactionTemp.createDate;
    transactionTemp.value=Number(transactionTemp.value)
    try {
        const newSalary:any=await restPost({url:"/transaction",payload:transactionTemp});
        setUserInfo({...userInfo,salary:newSalary.salary})
        setTransaction({
            userId: "",
            type: "Input",
            value: 0,
            description: ""
        })
        getTransactions()
    } catch (error) {
        console.log(error);
        
    }
}
export const updateTransaction=async ()=>{
    const {transaction,setUserInfo,userInfo}=useAppStore.getState()
    try {
        const newSalary:any=await restPut({url:`/transaction?transactionId=${transaction._id}`,payload:transaction});
        setUserInfo({...userInfo,salary:newSalary.salary})
        getTransactions()
    } catch (error) {
        console.log(error);
        
    }
}
export const deleteransaction=async ()=>{
    const {transaction,setUserInfo,userInfo}=useAppStore.getState()
    try {
        const newSalary:any=await restDelete({url:`/transaction?transactionId=${transaction._id}`});
        setUserInfo({...userInfo,salary:newSalary.salary})
        getTransactions()
    } catch (error) {
        console.log(error);
        
    }
}
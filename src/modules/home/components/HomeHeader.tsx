import { ImExit } from "react-icons/im";
import { logout } from "../../login/services/services.login";
import { useTranslation } from "react-i18next";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { ModeLanguage } from "../../../components/ModeLanguage";

export const HomeHeader = () => {
  const [t, i18n] = useTranslation("app")
  const showModalCloseSession = () => {
    withReactContent(Swal).fire({
      text: `${t("HOME.TXT_MODAL")}`,
      icon: "info",
      cancelButtonText: `${t("HOME.BTN_CANCEL")}`,
      confirmButtonText: `${t("HOME.BTN_OK")}`,
      showCancelButton: true, customClass: {
        confirmButton: "btn btn-primary mx-2",
        cancelButton: "btn btn-secondary mx-2"
      },
      buttonsStyling: false
    }).then((result) => {
      if (result.isConfirmed) {
        logout()
      }
    });
  }

  return (<div className="home-header__bar">
    <div className="flex">
      <p className="home-header__logo">{t("LOGO")}</p>
    </div>
    <div className="flex gap-3 mr-4 w-32 justify-end">
      <ModeLanguage/>
      <div className="tooltip tooltip-bottom self-center" data-tip={t("HOME.ICON_EXIT")}>
        <a className="self-center" onClick={showModalCloseSession}>
          <ImExit className="text-3xl text-white" />
        </a>
      </div>
    </div>
  </div>
  )
}
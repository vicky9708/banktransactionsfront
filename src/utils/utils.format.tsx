import moment from 'moment';
import i18next from "i18next";
const es = {
	months:
		"Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre".split(
			"_"
		),
	monthsShort:
		"Ene._Feb._Mar._Abr._May._Jun._Jul._Ago._Sept._Oct._Nov._Dic.".split("_"),
	weekdays: "Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado".split("_"),
	weekdaysShort: "Dom_Lun_Mar_Mier_Jue_Vier_Sab".split("_"),
	weekdaysMin: "Do_Lu_Ma_Mi_Ju_Vi_Sa".split("_"),
};
const en = {
	months:
		"January_February_March_April_May_June_July_August_September_Octubre_November_December".split(
			"_"
		),
	monthsShort:
		"Jan._Feb._Mar._Apr._May._Jun._Jul._Aug._Sep._Oct._Nov._Dec.".split("_"),
	weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split(
		"_"
	),
	weekdaysShort: "Sun._Mon._Tues._Wed._Thurs._Fri._Sat.".split("_"),
	weekdaysMin: "Sun._Mon._Tues._Wed._Thurs._Fri._Sat.".split("_"),
};
moment.locale("es", i18next.language === "en" ? en : es);

// Utilidades
type TypeFormatDate =
	| 'dddd'
	| 'DD [de] MMMM [de] YYYY'
	| 'DD/MM/YYYY'
	| 'DD/MM/YYYY HH:mm'
	| 'DD/MM/YYYY HH:mm:ss'
	| 'dddd, MMMM Do YYYY'
	| 'ddd, MMMM Do YYYY'
	| 'DD/MM/YYYY hh:mm A'
	| 'DD/MM/YYYY hh:mm:ss a'
	| 'DD/MM/YYYY HH:mm:ss a'
	| 'MM/YYYY'
	| 'WW/YYYY'
	| 'MMMM Do YYYY, h:mm:ss a'
	| 'MMMM Do YYYY, h:mm a'
	| 'HH:mm'
	| 'HH:mm:ss'
	| 'h:mm:ss a'
	| 'YYYY-MM-DDTHH:mm:ss'
	| 'YYYY';

const useFormatDate = (formats?: { formatIn?: TypeFormatDate; formatOut?: TypeFormatDate; }) => {
	const formatIn = formats?.formatIn || 'DD-MM-YYYY';
	const formatOut = formats?.formatOut || 'DD/MM/YYYY';

	const format = (value?: string | moment.Moment) => {
		let date = null;

		if (moment(value).format('DD/MM/YYYY') === moment().format('DD/MM/YYYY')) {
			date = i18next.language === "en" ? 'Today' : 'Hoy';
		} else if (
			moment(value).format('DD/MM/YYYY') === moment().subtract(1, 'days').format('DD/MM/YYYY') 
		) {
			date = i18next.language === "en" ? 'Yesterday' : 'Ayer';
		} else {
			date = value ? moment(value, formatIn).format(formatOut) : undefined;
		}

		return date;
	};

	const parser = (value?: string | moment.Moment): string | undefined =>
		value ? moment(value, formatOut).format(formatIn) : undefined;

	return { format, parser };
};
const useFormatNumber = (allOptions?: { prefix?: string; suffix?: string; toFixed?: number }) => {
	const format = (value?: number, individualOptions?: { prefix?: string; suffix?: string; toFixed?: number }): string | any => {
		const prefix = individualOptions?.prefix || allOptions?.prefix || '';
		const suffix = individualOptions?.suffix || allOptions?.suffix || '';
		if (value) {
			const _value = individualOptions?.toFixed !== undefined ? Number(value).toFixed(individualOptions.toFixed) : value;
			const _valueArr = `${_value}`.split(/[.,]+/g);
			const _valueInt = _valueArr[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
			const _valueMask = `${_valueInt}${_valueArr[1] ? `,${_valueArr[1]}` : ''}`;

			return `${prefix}${_valueMask}${suffix}`;
		}

		return '';
	};

	const parser = (value?: string): number | any => {
		if (value) {
			const _valueArr = `${value}`.split(',');
			const _valueInt = _valueArr[0].replace(/[^-0-9,]/g, '');
			const _valueUnMask = `${_valueInt}${_valueArr[1] ? `.${_valueArr[1]}` : ''}`;

			return Number(_valueUnMask);
		}

		return 0;
	};

	return { format, parser };
};

export { useFormatDate,useFormatNumber };

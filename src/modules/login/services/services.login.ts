import { restPost } from "../../../services/services.axios"
import { useAppStore } from "../../../store/store"
import { IFDataLogin, IFDataUser } from "../../../types/types.login"

const login=async (payload:IFDataLogin)=>{
   const {setAuthenticated,setUserInfo}=useAppStore.getState()
   try {
      const resp:any= await restPost({url:"/user/login",payload})
      setAuthenticated(true)
      setUserInfo(resp.user)
      window.location.href="/home"
   } catch (error) {
      console.log(error);      
   } 
      
}
const logout=()=>{
   sessionStorage.clear()
   window.location.href="/login"
}
const createUser=async ()=>{
   const user:IFDataUser={...useAppStore.getState().userInfo}
   delete user._id
   delete user.createDate
   user.salary=Number(user.salary)
   try {
      const resp:any= await restPost({url:"/user",payload:user})
      window.location.href="/login"
   } catch (error) {
      console.log(error);      
   }
}
export {login,logout,createUser}
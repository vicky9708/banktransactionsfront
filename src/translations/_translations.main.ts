// // Import I18Next
import { i18n } from "i18next";
// English Import
import app_en from "./en/en.app.json";
// Spanish Import
import app_es from "./es/es.app.json";

export function getTranslation(i18next: i18n) {
	const localLanguage = localStorage.getItem("language");
	const languageNav = window.navigator.language.substring(0, 2) ?? "en";
	!localLanguage
		? localStorage.setItem("language", languageNav)
		: localLanguage !== "es"
		? localStorage.setItem("language", "en")
		: localStorage.setItem("language", localLanguage);
	const Language = localStorage.getItem("language")?.toString();

	return i18next.init({
		interpolation: { escapeValue: false },
		lng: Language,
		resources: {
			es: {
				app: app_es,
			},
			en: {
				app: app_en,
			},
		},
	});
}

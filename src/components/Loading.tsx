//Import store
import { shallow } from "zustand/shallow";
//Import elements
import loading from "../assets/images/loading.svg"
import { useAppStore } from "../store/store";

export const Loading = () => {
	const { isLoading } = useAppStore(
		(state) => ({ isLoading: state.isLoading }),
		shallow
	);
	return (
		<>
			{isLoading && (
				<div className="app-loading">
					<img className="m-auto w-24" src={loading} />
				</div>
			)}
		</>
	);
};
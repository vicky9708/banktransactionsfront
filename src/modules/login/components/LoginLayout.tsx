import { useTranslation } from "react-i18next"
import { useState } from "react"
import { IFDataLogin } from "../../../types/types.login"
import { login } from "../services/services.login"
import header from "../../../assets/images/header.svg"
import { useNavigate } from "react-router-dom"
import { ModeLanguage } from "../../../components/ModeLanguage"
export const LoginLayout = () => {
    const { t } = useTranslation("app")
    const [dataLogin, setDataLogin] = useState<IFDataLogin>({ email: "", password: "" })
    const onChange = (event: any) => {
        const change = event.target
        setDataLogin({ ...dataLogin, [change.name]: change.value })
    }
    const onSubmit = (event: any) => {
        event.preventDefault()
        login(dataLogin)
    }
    const navigate = useNavigate()
    return (
        <div className="hero min-h-screen relative">
            <ModeLanguage className="top-1 right-4"/>
            <div className="hero-content flex-col lg:flex-row-reverse">
                <div className="card shrink-0 w-full max-w-sm shadow-2xl bg-base-100 relative">
                    <img className="rounded-t-2xl" src={header} />
                    <p className="login-logo">{t("LOGO")}</p>
                    <form className="card-body -mt-28" onSubmit={onSubmit}>
                        <div className="form-control">
                            <label className="label">
                                <span className="label-text">{t("LOGIN.LBL_EMAIL")}</span>
                            </label>
                            <input onChange={onChange} type="email" value={dataLogin.email} name="email" className="input input-bordered" required />
                        </div>
                        <div className="form-control">
                            <label className="label">
                                <span className="label-text">{t("LOGIN.LBL_PASSWORD")}</span>
                            </label>
                            <input onChange={onChange} type="password" name="password" value={dataLogin.password} className="input input-bordered" required />
                        </div>
                        <div className="form-control mt-6">
                            <button type="submit" className="btn btn-primary">{t("LOGIN.BTN_LOGIN")}</button>
                        </div>
                        <p className="!text-gray-700 text-xs text-center">{t("LOGIN.TXT_QUESTION")}
                        <a onClick={()=>{navigate("/register")}} className="cursor-pointer !text-secondary text-inherit underline">{t("LOGIN.TXT_LINK")}</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    )
}
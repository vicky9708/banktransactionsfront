import { shallow } from "zustand/shallow"
import { useAppStore } from "../store/store"
import { Route, Routes } from "react-router-dom"
import { RoutesPrivate } from "./RoutesPrivate"
import { RoutesPublic } from "./RoutesPublic"
import { getTranslation } from "../translations/_translations.main";
import { I18nextProvider, useTranslation } from "react-i18next";

import i18next from "i18next";
import { Loading } from "../components/Loading"

export const RoutesMain = () => {
    const { authenticated } = useAppStore((state) => ({ authenticated: state.authenticated }), shallow)
    getTranslation(i18next);
    return (
        <div className="app-background">
            <Loading />
            <I18nextProvider i18n={i18next}>            
                <Routes>
                    <Route
                        path="/*"
                        element={authenticated ? <RoutesPrivate /> : <RoutesPublic />}
                    />
                </Routes>
            </I18nextProvider>
        </div>

    )
}
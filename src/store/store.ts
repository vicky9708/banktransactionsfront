import { create } from 'zustand'
import { persist, createJSONStorage } from 'zustand/middleware'
import { IFDataUser, UserRole } from '../types/types.login'
import { IFTransaction, Transaction } from '../types/types.home'

export const useAppStore = create(
    persist(
        (set: any, get: any) => ({
            authenticated: false,
            setAuthenticated: (authenticated: boolean) => set({ authenticated }),
            userInfo: {
                name: "",
                phoneNumber: "",
                email: "",
                salary: 0,
                role: UserRole.CLIENT,
                password: "",
                _id: ""
            },
            setUserInfo: (userInfo: IFDataUser) => set({ userInfo }),
            transactions: [],
            setTransactions: (transactions: IFTransaction[]) => set({ transactions }),
            transaction: {
                userId: "",
                type: Transaction.INPUT,
                value: 0,
                description: "",
                _id:""
            },
            setTransaction: (transaction: IFTransaction) => set({ transaction }),
            isLoading:false,
            setIsLoading: (isLoading: boolean) => set({ isLoading }),
        }),
        {
            name: 'banking-transactions',
            storage: createJSONStorage(() => sessionStorage),
            partialize: (state: any) => ({
                authenticated: state.authenticated,
                userInfo: state.userInfo,
                transaction: state.transaction
            })
        },
    ),
)
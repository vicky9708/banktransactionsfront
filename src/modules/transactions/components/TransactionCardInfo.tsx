import { MdDelete, MdEdit } from "react-icons/md"
import { IFTransaction } from "../../../types/types.home"
import moment from "moment"
import Swal from "sweetalert2"
import withReactContent from "sweetalert2-react-content"
import { TransactionAddEdit } from "./TransactionAddEdit"
import { useAppStore } from "../../../store/store"
import { shallow } from "zustand/shallow"
import { deleteransaction } from "../services/services.transaction"
import { useFormatDate, useFormatNumber } from "../../../utils/utils.format"
import { useTranslation } from "react-i18next"
interface IFTransactionCardInfo {
  transaction: IFTransaction
}
export const TransactionCardInfo = (props: IFTransactionCardInfo) => {
  const { setTransaction } = useAppStore((state) => ({ setTransaction: state.setTransaction }), shallow)
  const { createDate, type, description, value } = props.transaction
  const { format: formatDate } = useFormatDate({ formatIn: "YYYY-MM-DDTHH:mm:ss", formatOut: "MMMM Do YYYY, h:mm a" })
  const { format: formatNumber } = useFormatNumber({ prefix: "$" })
  const {t}=useTranslation("app")
  const showModalCreate = () => {
    setTransaction(props.transaction)
    withReactContent(Swal).fire({
      title: <i className="!text-gray-700">{t("TRANSACTION.MODAL_EDIT_CREATE.TITLE")}</i>,
      html: <TransactionAddEdit translation={t} action="Edit" />,
      showConfirmButton:false,
      showCancelButton:false,
      showCloseButton:true
    })
    
  }
  const showModalDelete = () => {
    setTransaction(props.transaction)
      withReactContent(Swal).fire({
        text:`${t("TRANSACTION.MODAL_DELETE.TXT_DESCRIPTION")}`,
        icon:"warning",
        cancelButtonText:`${t("TRANSACTION.MODAL_DELETE.BTN_CANCEL")}`,
        confirmButtonText: `${t("TRANSACTION.MODAL_DELETE.BTN_OK")}`,  
        showCancelButton:true,customClass: {
          confirmButton: "btn btn-primary mx-2",
          cancelButton: "btn btn-secondary mx-2"
        },
        buttonsStyling: false                    
      }).then((result) => {
        if (result.isConfirmed) {
          deleteransaction()
        }
      });
    
  }
  return (<>
    <div className="flex justify-between border-b-[1px] p-4 border-gray-300">
      <div className="w-full block ml-4">
        <p className="sm:text-lg text-sm font-bold">{formatDate(String(createDate))}</p>
        <p className="sm:text-md text-xs">{description}</p>
      </div>
      <div className="w-full flex self-center justify-end ">
        <p className={`sm:text-2xl text-lg ${type === "Input" ? "!text-primary" : "!text-error"}`}>{type === "Input" ? "+" : "-"}{formatNumber(value)}</p>
      </div>
      <div className="w-full justify-end sm:mr-4 mr-0 flex self-center gap-2">
        <div className="tooltip tooltip-bottom" data-tip={t("TRANSACTION.ICON_EDIT")}>
          <a className="rounded-full w-9 h-9 bg-gray-300 flex cursor-pointer">
            <MdEdit onClick={showModalCreate} className="text-xl hover:text-2xl text-gray-700 m-auto" />
          </a></div>
        <div className="tooltip tooltip-bottom" data-tip={t("TRANSACTION.ICON_DELETE")}>
          <a className="rounded-full w-9 h-9 bg-gray-300 flex cursor-pointer">
            <MdDelete onClick={showModalDelete} className="text-xl hover:text-2xl text-gray-700 m-auto" />
          </a></div>
      </div>
    </div>
  </>)
}



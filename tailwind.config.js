/** @type {import('tailwindcss').Config} */
import daisyui from "daisyui"
export default {
  darkMode: 'class',
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [daisyui],
  daisyui: {
    themes: [
      {
        mytheme: {
          "primary": "#00c79d",
          "secondary": "#0090ff",
          "accent": "#009100",
          "neutral": "#1f2624",
          "base-100": "#f0fff9",
          "info": "#00b8ff",
          "success": "#80df00",
          "warning": "#ffad00",
          "error": "#ff6a8b",
        }
      }          
      
    ],
  },
  themes: [
    {

    },
  ],
}


import { shallow } from "zustand/shallow"
import { useAppStore } from "../../../store/store"
import { useTranslation } from "react-i18next"
import { createUser } from "../services/services.login"
import { useNavigate } from "react-router-dom"
import { ModeLanguage } from "../../../components/ModeLanguage"

export const LoginRegister = () => {
    const { t } = useTranslation("app")
    const { userInfo, setUserInfo } = useAppStore((state) => ({ setUserInfo: state.setUserInfo, userInfo: state.userInfo }), shallow)
    const onSubmit = (event: any) => {
        event.preventDefault()
        createUser()
    }
    const onChange = (event: any) => {
        const change = event.target
        setUserInfo({ ...userInfo, [change.name]: change.value })
    }
    const navigate = useNavigate()
    return (<>
        <div className="hero min-h-screen relative">
        <ModeLanguage className="top-1 right-4"/>
            <div className="hero-content flex-col lg:flex-row-reverse w-[377px]">
                <div className="card shrink-0 w-full max-w-sm shadow-2xl bg-base-100 relative">
                    <h1 className="text-[30px] !text-gray-600 text-center mt-4">{t("REGISTER.TITLE")}</h1>
                    <form className="card-body " onSubmit={onSubmit}>
                        <div className="overflow-y-auto sm:h-auto h-[50vh]">
                            <div className="form-control">
                                <label className="label">
                                    <span className="label-text">{t("REGISTER.LBL_NAME")}</span>
                                </label>
                                <input onChange={onChange} type="text" value={userInfo.name} name="name" className="input input-bordered" required />
                            </div>
                            <div className="form-control">
                                <label className="label">
                                    <span className="label-text">{t("REGISTER.LBL_PHONE")}</span>
                                </label>
                                <input onChange={onChange} type="text" value={userInfo.phoneNumber} name="phoneNumber" className="input input-bordered" required />
                            </div>
                            <div className="form-control">
                                <label className="label">
                                    <span className="label-text">{t("REGISTER.LBL_EMAIL")}</span>
                                </label>
                                <input autoComplete="off" onChange={onChange} type="email" value={userInfo.email} name="email" className="input input-bordered" required />
                            </div>
                            <div className="form-control">
                                <label className="label">
                                    <span className="label-text">{t("REGISTER.LBL_CAPITAL")}</span>
                                </label>
                                <input onChange={onChange} type="number" value={userInfo.salary} name="salary" className="input input-bordered" required />
                            </div>
                            <div className="form-control">
                                <label className="label">
                                    <span className="label-text">{t("REGISTER.LBL_PASSWORD")}</span>
                                </label>
                                <input autoComplete="off" onChange={onChange} type="password" value={userInfo.password} name="password" className="input input-bordered" required />
                            </div>
                        </div>
                        <div className="form-control mt-6">
                            <button type="submit" className="btn btn-primary">{t("REGISTER.BTN_REGISTER")}</button>
                        </div>
                        <p className="!text-gray-700 text-xs text-center">{t("REGISTER.TXT_QUESTION")}
                            <a onClick={() => { navigate("/login") }} className="cursor-pointer !text-secondary text-inherit underline">{t("REGISTER.TXT_LINK")}</a>
                        </p>
                    </form>

                </div >
            </div>
        </div>
    </>)



}

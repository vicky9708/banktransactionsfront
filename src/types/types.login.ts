export interface IFDataLogin {
    email: string;
    password: string
}
export interface IFDataUser {
    name: string,
    phoneNumber: string,
    email: string,
    salary: number,
    role: TypeRole,
    password: string,
    _id?: string
    createDate?: Date,
}
type TypeRole = "Admin" | "Client"
export enum UserRole{
    ADMIN="Admin",
    CLIENT="Client"
}
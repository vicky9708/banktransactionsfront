import { restGet } from "../../../services/services.axios"
import { useAppStore } from "../../../store/store"

const getTransactions=async ()=>{
    const {userInfo,setTransactions}=useAppStore.getState()
try {
    const transactions:any=await restGet({url:`/transaction?userId=${userInfo._id}`})    
    setTransactions(transactions.transactions)
} catch (error) {
    console.log(error)
    setTransactions([])
}
}
export {getTransactions}
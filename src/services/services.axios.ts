import axios from 'axios';
import { useAppStore } from '../store/store';

const instance = axios.create({
  baseURL: import.meta.env.VITE_ENDPOINT,
  headers: { 'Content-type': 'application/json' }
});
export const getConfig = () => {

  const data = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  return data;
};
instance.interceptors.request.use(
  async (config: any) => {
    config.headers = getConfig().headers;
    useAppStore.getState().setIsLoading(true)
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);
instance.interceptors.response.use(
  async (response) => {
    useAppStore.getState().setIsLoading(false)
    return response.data;
  },
  async function (error) {
    useAppStore.getState().setIsLoading(false)
    return Promise.reject(error);
  }
);

const restPost = async (payload: any) => {
  return instance.post(payload.url, payload.payload);
};

const restGet = async (payload: any) => {
  return instance.get(payload.url);
};

const restPut = async (payload: any) => {
  return instance.put(payload.url, payload.payload);
};
const restDelete = async (payload: any) => {
  return instance.delete(payload.url);
};
export { restPost, restGet, restPut, restDelete }



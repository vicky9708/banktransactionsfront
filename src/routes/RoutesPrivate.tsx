import { Route, Routes } from "react-router-dom"
import { Home } from "../views/Home"
import { HomeHeader } from "../modules/home/components/_home.main"

export const RoutesPrivate = () => {
	return (<>
		<HomeHeader />
		<Routes>
			<Route path="/home" element={<Home />} />
			<Route path="*" element={<Home />} />
		</Routes>
	</>

	)
}
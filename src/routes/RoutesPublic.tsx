import { Route, Routes } from "react-router-dom"
import { Login } from "../views/Login"
import { LoginRegister } from "../modules/login/components/_login.main"

export const RoutesPublic = () => {
	return (
		<Routes>
			<Route path="/login" element={<Login />} />
			<Route path="/register" element={<LoginRegister />} />
			<Route path="*" element={<Login />} />
		</Routes>
	)
}